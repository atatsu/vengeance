from abc import abstractmethod, ABCMeta


class ProtocolException(Exception):
	"""
	Base exception class for implemented protocols. Isn't really intended
	to be used directly. Really just something to tie all the other exceptions
	to.
	"""


class ConnectionSuccess(ProtocolException):
	"""
	Should be raised when a connection attept proves successful.
	"""


class ConnectionFailure(ProtocolException):
	"""
	Should be raised when a connection attempt bombs out.
	"""


class AuthenticationNotRequired(ProtocolException):
	"""
	Should be raised when the implemented protocol doesn't (for whatever
	friggin' reason) require authentication.
	"""


class AuthenticationSuccess(ProtocolException):
	"""
	Should be raised when the authentication process proves successful.
	"""


class AuthenticationFailure(ProtocolException):
	"""
	Should be raised when someone fat-fingers the server auth password.
	"""


class Protocol(metaclass=ABCMeta):
	"""
	Base class for all protocols. A protocol is responsible for establishing a connection
	to the target server (so it needs to know how to communicate with it). It also needs
	to provide a mechanism for authenticating if the target server requires authentication.
	"""
	_authenticated = False
	_connected = False

	@property
	def is_connected(self):
		return self._connected

	@property
	def is_authenticated(self):
		return self._authenticated

	@abstractmethod
	async def authenticate(self, *, password: str = None):
		"""
		What good is establishing a connection if we can't authenticate? Few and far between
		is a server setup that doesn't require authentication. That isn't to say they don't
		exist (wha wha wha???), but, c'mon, seriously.

		In the unlikely event the server you're wanting to administer does *not* require
		an authentication step, there's something you can raise! Simply eject an
		`AuthenticationNotRequired` and you're good to go.

		Assuming authentication is required (let's hope so) it is very similar to
		establishing a connection. A set of exceptions to throw to reflect the state.

		If authentication is successful simply raise an `AuthenticationSuccess`. Conversely,
		if authentication fails, raise an `AuthenticationFailure`.

		In short:
			On success: `raise AuthenticationSuccess`
			On failure: `raise AuthenticationFailure`
			No authentication: `raise AuthenticationNotRequired`
		"""

	@abstractmethod
	async def connect(self, *, host: str = None, port: int = None):
		"""
		Every protocol is assumed to require a host and port in order to connect.
		This assumption should be obvious enough that it requires no explaining.

		In the event a connection attempt fails it is the responsibility of the implementer
		to raise a `ConnectionFailure` exception.

		If the connection succeeds it is the responsibility of the implementer to raise
		a `ConnectionSuccess` exception.

		Raising an exception when the desired result
		is achieved? But that's lunacy! aiohttp (https://github.com/aio-libs/aiohttp)
		takes this approach and it works really well. So... yeah. That's the nice thing
		about writing libraries/frameworks, you get to make these kinds of decisions!
		Whether or not people actually use the stuff you write, well, that's an entirely
		different matter.

		At any time during the execution of the implemented protocol's code, if the connection
		should ever be dropped, for whatever reason, it is the responsibility of the implementer
		to raise... you guessed it: a `ConnectionFailure` exception.

		The `botman` library does its best to manage the connection state for you, and in
		order to do this effectively you need to provide it some clues (spoon-fed clues,
		mind you).

		In short:
			On success: `raise ConnectionSuccess`
			On failure: `raise ConnectionFailure`
			On disconnect: `raise ConnectionFailure`
		"""

	@abstractmethod
	async def read(self) -> bytes:
		"""
		Read shit.
		"""

	@abstractmethod
	async def write(self, data: bytes):
		"""
		Write shit.
		"""

	async def _try_connect(self, *, host=None, port=None):
		try:
			await self.connect(host=host, port=port)
		except ConnectionFailure as ex:
			self._connected = False
		except ConnectionSuccess as ex:
			self._connected = True

	async def _try_authenticate(self, *, password=None):
		try:
			await self.authenticate(password=password)
		except AuthenticationFailure as ex:
			self._authenticated = False
		except AuthenticationSuccess as ex:
			self._authenticated = True
		except AuthenticationNotRequired as ex:
			self._authenticated = True
