import pytest


from botman.protocol import (
	AuthenticationFailure,
	AuthenticationNotRequired,
	AuthenticationSuccess,
	ConnectionFailure,
	ConnectionSuccess,
	Protocol
)


class MyProtocolImplementation(Protocol):

	async def connect(self, host, port):
		if host == 'fail':
			raise ConnectionFailure()
		raise ConnectionSuccess()

	async def authenticate(self, password):
		if password == 'fail':
			raise AuthenticationFailure()
		if password == 'pass':
			raise AuthenticationSuccess()
		raise AuthenticationNotRequired()

	async def read(self):
		pass

	async def write(self):
		pass


class TestProtocolConnectionStateHandling:

	@pytest.fixture(autouse=True)
	def setup(self):
		self.protocol = MyProtocolImplementation()
		assert self.protocol.is_connected is False

	@pytest.mark.asyncio
	async def test_connection_failure(self):
		await self.protocol._try_connect(host='fail', port=8000)
		assert self.protocol.is_connected is False

	@pytest.mark.asyncio
	async def test_connection_success(self):
		await self.protocol._try_connect(host='success', port=8000)
		assert self.protocol.is_connected is True

	@pytest.mark.asyncio
	async def test_auth_failure(self):
		await self.protocol._try_authenticate(password='fail')
		assert self.protocol.is_authenticated is False

	@pytest.mark.asyncio
	async def test_auth_success(self):
		await self.protocol._try_authenticate(password='pass')
		assert self.protocol.is_authenticated is True

	@pytest.mark.asyncio
	async def test_auth_none(self):
		await self.protocol._try_authenticate(password=None)
		assert self.protocol.is_authenticated is True
